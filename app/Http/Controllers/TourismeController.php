<?php

namespace App\Http\Controllers;

use App\Tourisme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TourismeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tourisme::All();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         Validator::make($request->all(),[
            'titre' => 'required|max:255',
            'description' => 'required',
            'lieu' =>  'required|max:255',
            'pays' => 'required|max:255'
        ]);

        $tourisme = new Tourisme;
        $tourisme->titre = $request->titre;
        $tourisme->description = $request->description;
        $tourisme->lieu = $request->lieu;
        $tourisme->pays = $request->pays;
        $tourisme->save();

        return "Ajout OK";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tourisme  $tourisme
     * @return \Illuminate\Http\Response
     */
    public function show(Tourisme $tourisme)
    {
        $requete = Tourisme::findOrFail($tourisme->id);

        return $requete;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tourisme  $tourisme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tourisme $tourisme)
    {
        $requete = Tourisme::findOrFail($tourisme->id);

        $requete->titre = $request->titre;
        $requete->description = $request->description;
        $requete->lieu = $request->lieu;
        $requete->pays = $request->pays;
        $requete->save();

        return "Modification OK";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tourisme  $tourisme
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tourisme $tourisme)
    {
        $requete = Tourisme::findOrFail($tourisme->id);
        $requete->delete();

        return "Elément supprimé";
    }
}
